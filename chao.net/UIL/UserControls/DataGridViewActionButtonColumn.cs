﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using chao.net.UIL.UserControls;

namespace chao.net.UIL
{
    /// <summary>
    /// 对象名称：DataGridView操作按钮列（修改、删除）
    /// 对象说明：通过本类可以实现，在DataGridView控件中，显示一个分别包含修改、删除、详情两个图片按钮的列。
    /// 作者姓名：ken
    /// 编写日期：2020/09/12 
    /// </summary>
    public class DataGridViewActionButtonColumn : DataGridViewColumn
    {
        private List<ActionButton> buttonList;

        public DataGridViewActionButtonColumn()
        {
            DataGridViewActionButtonCell dataGridViewActionButtonCell = new DataGridViewActionButtonCell();

            List<ActionButton> list = new List<ActionButton>();
            list.Add(ActionButtonDefaultConfig.GetUpdateBtnConfig());
            list.Add(ActionButtonDefaultConfig.GetDeleteBtnConfig());
            list.Add(ActionButtonDefaultConfig.GetDetailBtnConfig());

            buttonList = list;

            DataGridViewActionButtonCell.ButtonList = buttonList;

            this.CellTemplate = dataGridViewActionButtonCell;
            this.HeaderText = "操作";
        }

        internal List<ActionButton> ButtonList { get => buttonList; set => buttonList = value; }
    }
}

