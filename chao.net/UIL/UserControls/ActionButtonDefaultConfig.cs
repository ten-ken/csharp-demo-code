﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace chao.net.UIL.UserControls
{
     class ActionButtonDefaultConfig
    {
        public static ActionButton GetUpdateBtnConfig()
        {
            ActionButton actionButton = new ActionButton();

            actionButton.Name = "UPDATE";

            Image btnImage = Properties.Resources.BtnModify;
           
            Image btnImageChecked = Properties.Resources.BtnModify02;


            Pen penBtn = new Pen(Color.FromArgb(135, 163, 193)); // 按钮边框颜色

            Pen penBtnChecked = new Pen(Color.FromArgb(162, 144, 77)); // 按钮边框颜色--鼠标悬浮

            actionButton.BtnImage = btnImage;

            actionButton.BtnImageChecked = btnImageChecked;

            actionButton.BtnPen = penBtn;

            actionButton.BtnPenChecked = penBtnChecked;

            return actionButton;
        }

        public static ActionButton GetDeleteBtnConfig()
        {
            ActionButton actionButton = new ActionButton();

            actionButton.Name = "DELETE";

            Image btnImage = Properties.Resources.BtnDelete;

            Image btnImageChecked = Properties.Resources.BtnDelete02;

            Pen penBtn = new Pen(Color.FromArgb(135, 163, 193)); // 按钮边框颜色

            Pen penBtnChecked = new Pen(Color.FromArgb(162, 144, 77)); // 按钮边框颜色--鼠标悬浮

            actionButton.BtnImage = btnImage;

            actionButton.BtnImageChecked = btnImageChecked;

            actionButton.BtnPen = penBtn;

            actionButton.BtnPenChecked = penBtnChecked;

            return actionButton;
        }

        public static ActionButton GetDetailBtnConfig()
        {
            ActionButton actionButton = new ActionButton();

            actionButton.Name = "DETAIL";

            Image btnImage = Properties.Resources.BtnView;

            Image btnImageChecked = Properties.Resources.BtnView02;

            Pen penBtn = new Pen(Color.FromArgb(135, 163, 193)); // 按钮边框颜色

            Pen penBtnChecked = new Pen(Color.FromArgb(162, 144, 77)); // 按钮边框颜色--鼠标悬浮

            actionButton.BtnImage = btnImage;

            actionButton.BtnImageChecked = btnImageChecked;

            actionButton.BtnPen = penBtn;

            actionButton.BtnPenChecked = penBtnChecked;

            return actionButton;
        }
    }
}
