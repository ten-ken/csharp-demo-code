﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace chao.net
{
    public partial class FormTest3 : Form
    {
        public FormTest3()
        {
            InitializeComponent();
        }

        private void FormTest3_Load(object sender, EventArgs e)
        {
            this.dataGridView1.AutoGenerateColumns = false; ;
            this.dataGridView1.MouseClick += dataGridView1_MouseClick;
            this.dataGridView1.Rows.Add();
        }



        /// <summary>
        /// stripItem0点击事件-删除选中行
        /// </summary>
        private void stripItems0_Click(object sender, EventArgs e)
        {
            MessageBox.Show("-删除选中行--");
        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            ContextMenuStrip strip = new ContextMenuStrip();
            strip.ShowImageMargin = false;//设置右击菜单属性
            strip.Items.Add("删除选定行");
            strip.Items.Add("添加行");

            strip.Items[0].Click += stripItems0_Click;//弹出菜单第一项点击事件
            strip.Items[1].Click += stripItems1_Click;//弹出菜单第二项点击事件

            if (e.Button == MouseButtons.Right)//右键点击
            {
                strip.Show(this.dataGridView1, e.Location);//弹出菜单
            }
        }

        /// <summary>
        /// 弹出菜单第二项点击事件
        /// </summary>
        private void stripItems1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("-添加行-");
        }

    }
}
